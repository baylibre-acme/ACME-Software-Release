# ACME Releases

Releases are signed with the following keys :
```
pub  2048R/A4CFF8AE 2011-02-14 Neil Armstrong <narmstrong@baylibre.com>
                               Neil Armstrong <superna@na-prod.com>
                               Superna <superna@na-prod.com>
                               Neil Armstrong <arm.neil@na-prod.com>
                               Neil Armstrong <narmstrong@kernel.org>
                               Neil "Superna" Armstrong <superna@na-prod.com>
                               Neil "Superna" Armstrong <superna9999@gmail.com>
                               [user attribute packet]
```

## b7

  * [acme-beaglebone-black_b7-sdcard-image.xz](acme-beaglebone-black_b7-sdcard-image.xz) ([SHA256](acme-beaglebone-black_b7-sdcard-image.xz.sha256sum))

### Download

```
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b7-sdcard-image.xz
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b7-sdcard-image.xz.sha256sum
```

### Check File

```
sha256sum -c acme-beaglebone-black_b5-sdcard-image.xz.sha256sum
```

### Copy on SDCard

Considering /dev/mmcblk0 is your SDCard block device.

```
xzcat acme-beaglebone-black_b7-sdcard-image.xz | sudo dd of=/dev/mmcblk0
```

## b5

  * [acme-beaglebone-black_b5-sdcard-image.xz](acme-beaglebone-black_b5-sdcard-image.xz) ([GPG Signature](acme-beaglebone-black_b5-sdcard-image.xz.sig), [SHA256](acme-beaglebone-black_b5-sdcard-image.xz.sha256sum))
  * [acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.sh](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.sh) ([Host Manifest](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.host.manifest), [Target Manifest](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.target.manifest))

### Download

```
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b5-sdcard-image.xz
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b5-sdcard-image.xz.sig
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b5-sdcard-image.xz.sha256sum
```

### Check File

```
gpg --keyserver keyserver.ubuntu.com --recv A4CFF8AE
gpg --verify acme-beaglebone-black_b5-sdcard-image.xz.sig
sha256sum -c acme-beaglebone-black_b5-sdcard-image.xz.sha256sum
```

### Copy on SDCard

Considering /dev/mmcblk0 is your SDCard block device.

```
xzcat acme-beaglebone-black_b5-sdcard-image.xz | sudo dd of=/dev/mmcblk0
```

## b4

  * [acme-beaglebone-black_b4-sdcard-image.xz](acme-beaglebone-black_b4-sdcard-image.xz) ([GPG Signature](acme-beaglebone-black_b4-sdcard-image.xz.sig), [SHA256](acme-beaglebone-black_b4-sdcard-image.xz.sha256sum))
  * [acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.sh](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.sh) ([Host Manifest](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.host.manifest), [Target Manifest](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.target.manifest))

### Download

```
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b4-sdcard-image.xz
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b4-sdcard-image.xz.sig
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b4-sdcard-image.xz.sha256sum
```

### Check File

```
gpg --keyserver keyserver.ubuntu.com --recv A4CFF8AE
gpg --verify acme-beaglebone-black_b4-sdcard-image.xz.sig
sha256sum -c acme-beaglebone-black_b4-sdcard-image.xz.sha256sum
```

### Copy on SDCard

Considering /dev/mmcblk0 is your SDCard block device.

```
xzcat acme-beaglebone-black_b4-sdcard-image.xz | sudo dd of=/dev/mmcblk0
```

## b3

  * [acme-beaglebone-black_b3-sdcard-image.xz](acme-beaglebone-black_b3-sdcard-image.xz) ([GPG Signature](acme-beaglebone-black_b3-sdcard-image.xz.sig), [SHA256](acme-beaglebone-black_b3-sdcard-image.xz.sha256sum))
  * [acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.sh](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.sh) ([Host Manifest](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.host.manifest), [Target Manifest](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.3.target.manifest))

### Download

```
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b3-sdcard-image.xz
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b3-sdcard-image.xz.sig
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b3-sdcard-image.xz.sha256sum
```

### Check File

```
gpg --keyserver keyserver.ubuntu.com --recv A4CFF8AE
gpg --verify acme-beaglebone-black_b3-sdcard-image.xz.sig
sha256sum acme-beaglebone-black_b3-sdcard-image.xz.sha256sum
```

### Copy into SDCard

Considering /dev/mmcblk0 is your SDCard block device.

```
xzcat acme-beaglebone-black_b3-sdcard-image.xz | sudo dd of=/dev/mmcblk0
```

## b2

  * [acme-beaglebone-black_b2-sdcard-image.xz](acme-beaglebone-black_b2-sdcard-image.xz) ([GPG Signature](acme-beaglebone-black_b2-sdcard-image.xz.sig), [SHA256](acme-beaglebone-black_b2-sdcard-image.xz.sha256sum))
  * [acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.2.sh](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.2.sh) ([Host Manifest](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.2.host.manifest), [Target Manifest](acme-baylibre-glibc-x86_64-baylibre-acme-image-cortexa8hf-neon-toolchain-2.1.2.target.manifest))

### Download

```
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b2-sdcard-image.xz
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b2-sdcard-image.xz.sig
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b2-sdcard-image.xz.sha256sum
```

### Check File

```
gpg --keyserver keyserver.ubuntu.com --recv A4CFF8AE
gpg --verify acme-beaglebone-black_b2-sdcard-image.xz.sig
sha256sum acme-beaglebone-black_b2-sdcard-image.xz.sha256sum
```

### Copy into SDCard

Considering /dev/mmcblk0 is your SDCard block device.

```
xzcat acme-beaglebone-black_b2-sdcard-image.xz | sudo dd of=/dev/mmcblk0
```

## b1

  * [acme-beaglebone-black_b1-sdcard-image.xz](acme-beaglebone-black_b1-sdcard-image.xz) ([GPG Signature](acme-beaglebone-black_b1-sdcard-image.xz.sig), [SHA256](acme-beaglebone-black_b1-sdcard-image.xz.sig))

### Download

```
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b1-sdcard-image.xz
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b1-sdcard-image.xz.sig
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b1-sdcard-image.xz.sha256sum
```

### Check File

```
gpg --keyserver keyserver.ubuntu.com --recv A4CFF8AE
gpg --verify acme-beaglebone-black_b1-sdcard-image.xz.sig
sha256sum acme-beaglebone-black_b1-sdcard-image.xz.sha256sum
```

### Copy into SDCard

Considering /dev/mmcblk0 is your SDCard block device.

```
xzcat acme-beaglebone-black_b1-sdcard-image.xz | sudo dd of=/dev/mmcblk0
```
## b0

  * [acme-beaglebone-black_b0-sdcard-image.xz](acme-beaglebone-black_b0-sdcard-image.xz) ([GPG Signature](acme-beaglebone-black_b0-sdcard-image.xz.sig), [SHA256](acme-beaglebone-black_b0-sdcard-image.xz.sig))

### Download

```
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b0-sdcard-image.xz
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b0-sdcard-image.xz.sig
wget https://gitlab.com/baylibre-acme/ACME-Software-Release/raw/master/acme-beaglebone-black_b0-sdcard-image.xz.sha256sum
```

### Check File

```
gpg --keyserver keyserver.ubuntu.com --recv A4CFF8AE
gpg --verify acme-beaglebone-black_b0-sdcard-image.xz.sig
sha256sum acme-beaglebone-black_b0-sdcard-image.xz.sha256sum
```

### Copy into SDCard

Considering /dev/mmcblk0 is your SDCard block device.

```
xzcat acme-beaglebone-black_b0-sdcard-image.xz | sudo dd of=/dev/mmcblk0
```
